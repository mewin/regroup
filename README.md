# ReGroup

**This project is WIP, not everything works yet. Also expect bugs.**

World Of Warcraft addon to manage your (Raid) groups. Add/remove players, assign roles and invite them all at once.

## Features

 * [x] create and manage groups of characters
 * [x] import multiple characters from friends/guild/current party
 * [ ] automatically assign roles when players join the party
 * [ ] automatically assign raid assistant/main tank roles
 * [ ] option to auto invite players when they start the game

## Getting Started

Download the plugin and copy the files to `<World Of Warcraft Installation>\_retail_\Interface\AddOns\ReGroup`.

## Versioning

For the versions available, see the [tags on this repository](https://gitlab.com/mewin/regroup/tags).

## Authors

* **Patrick Wuttke** - *Initial work* - [mewin](https://mewin.de)

See also the list of [contributors](https://gitlab.com/mewin/regroup/graphs/master) who participated in this project.

## License

This project is published under the terms of the MIT license. See the LICENSE file for more information.
