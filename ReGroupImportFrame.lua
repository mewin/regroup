

-- Import Members functions
function ReGroup.PopulateFromList()
    ReGroup.ClearTable("importList")

    if ReGroup.importFrom == ReGroup.IMPORT_FROM_FRIENDS then
        for i = 1, C_FriendList.GetNumFriends() do
            local info = C_FriendList.GetFriendInfoBIndex(i)
            if info and info.className then
                local entry = {
                    name = info.name,
                    class = ReGroup.REVERSE_CLASS_NAMES[info.className],
                    race = "UNKNOWN"
                }
                table.insert(ReGroup.importList, entry)
            end
        end

        for i = 1, BNGetNumFriends() do
            local info = C_BattleNet.GetFriendAccountInfo(i).gameAccountInfo
            if info and info.characterName then -- is currently online
                local entry = {
                    name = info.characterName .. "-" .. info.realmName,
                    class = ReGroup.REVERSE_CLASS_NAMES[info.className],
                    race = ReGroup.REVERSE_RACE_NAMES[info.raceName]
                }
                table.insert(ReGroup.importList, entry)
            end
        end
    elseif ReGroup.importFrom == ReGroup.IMPORT_FROM_GROUP then
        if IsInRaid() then
            for i = 1, 40 do
                local unitid = "raid" .. i
                if UnitIsPlayer(unitid) then
                    local name, realm = UnitFullName(unitid)
                    local _, class = UnitClass(unitid)
                    local _, race = UnitRace(unitid)

                    if not realm or realm == "" then
                        realm = GetRealmName()
                    end

                    local entry = {
                        name = name .. "-" .. realm,
                        class = class,
                        race = race
                    }
                    table.insert(ReGroup.importList, entry)
                end
            end
        else
            for i = 1, 4 do
                local unitid = "party" .. i
                if UnitIsPlayer(unitid) then
                    local name, realm = UnitFullName(unitid)
                    local _, class = UnitClass(unitid)
                    local _, race = UnitRace(unitid)

                    if not realm or realm == "" then
                        realm = GetRealmName()
                    end

                    local entry = {
                        name = name .. "-" .. realm,
                        class = class,
                        race = race
                    }
                    table.insert(ReGroup.importList, entry)
                end
            end
        end
    elseif ReGroup.importFrom == ReGroup.IMPORT_FROM_GUILD then
        for i = 1, GetNumGuildMembers() do
            local name, _, _, level, _, _, _, _, _, _, class = GetGuildRosterInfo(i)
            local entry = {
                name = name,
                class = class,
                race = "UNKNOWN"
            }
            table.insert(ReGroup.importList, entry)
        end
    elseif ReGroup.importFrom > ReGroup.IMPORT_FROM_CLUB then
        local club = ReGroup.clubs[ReGroup.importFrom - ReGroup.IMPORT_FROM_CLUB]
        local members = C_Club.GetClubMembers(club.clubId)

        for _, memberId in ipairs(members) do
            local memberInfo = C_Club.GetMemberInfo(club.clubId, memberId)
            if not memberInfo.isSelf and memberInfo.presence == Enum.ClubMemberPresence.Online then
                -- print(memberInfo.name, " ", memberInfo.guid)
            end
        end
    end

    table.sort(ReGroup.importList, function(member0, member1)
        return member0.name < member1.name
    end)
end

-- sidebar
function ReGroup.SortDropDown_OnLoad()
    info            = UIDropDownMenu_CreateInfo();

    for index, option in ipairs(ReGroup.SORT_BY_OPTIONS) do
        info.text = option.text
        info.checked = index == ReGroupPersistent.sortBy
        info.func = function()
            ReGroupPersistent.sortBy = index
            ReGroup.UpdateSortedMembers()
            ReGroup.UpdateFrame()
        end
        UIDropDownMenu_AddButton(info)
    end
end

-- Import Members Frame
function ReGroup.ImportMembersDropDownButton_OnLoad()
    info            = UIDropDownMenu_CreateInfo();

    for from, name in ipairs(ReGroup.IMPORT_FROM_LABELS) do
        info.checked = ReGroup.importFrom == from
        info.text = name
        info.func = function()
            ReGroup.ImportMembersFrame_SetFrom(from)
        end
        UIDropDownMenu_AddButton(info)
    end

    do return end

    -- doesnt work...
    ReGroup.clubs = C_Club.GetSubscribedClubs()
    for i, club in ipairs(ReGroup.clubs) do
        info.checked = ReGroup.importFrom == ReGroup.IMPORT_FROM_CLUB + i
        info.text = '"' .. club.name .. '"'
        info.func = function()
            ReGroup.ImportMembersFrame_SetFrom(ReGroup.IMPORT_FROM_CLUB + i)
        end
        UIDropDownMenu_AddButton(info)
    end
end

function ReGroup.ImportMembersFrame_Update()
    ReGroup.ImportMembersFrame_ScrollFrame_Update()

    local anyChecked = false
    for _, player in pairs(ReGroup.importList) do
        if player.checked then
            anyChecked = true
            break
        end
    end
    if anyChecked then
        ReGroup_ImportMembersFrame_ButtonImport:Enable()
    else
        ReGroup_ImportMembersFrame_ButtonImport:Disable()
    end
end

function ReGroup.ImportMembersFrame_SetFrom(from)
    ReGroup.importFrom = from
    ReGroup.PopulateFromList()
    UIDropDownMenu_SetText(ReGroup_ImportMembersFrame_DropDown, ReGroup.IMPORT_FROM_LABELS[from] or "")
    ReGroup.ImportMembersFrame_Update()
end

function ReGroup.ImportMembersFrame_ScrollFrame_Update()
    if not ReGroup_ImportMembersFrame:IsVisible() then return end

    local scrollFrame = ReGroup_ImportMembersFrame_ScrollFrame
	local offset = HybridScrollFrame_GetOffset(scrollFrame)
	local buttons = scrollFrame.buttons
	local numButtons = #buttons
	local numPlayerButtons = #ReGroup.importList

    local usedHeight = 0
    local height = 34
    local width = scrollFrame:GetWidth()

	for i = 1, numButtons do
		local button = buttons[i]
		local index = offset + i
		if index <= numPlayerButtons then
			button.index = index
            ReGroup.ImportMembersFrame_ScrollFrame_UpdateButton(button)

            button:SetHeight(height)
            button:SetWidth(width)
			usedHeight = usedHeight + height
			button:Show()
		else
			button.index = nil
			button:Hide()
		end
	end
	HybridScrollFrame_Update(scrollFrame, numPlayerButtons * 34, usedHeight)
end

function ReGroup.ImportMembersFrame_ScrollFrame_UpdateButton(button)
    local index = button.index
    local player = ReGroup.importList[index]
    local color = RAID_CLASS_COLORS[player.class] or WHITE
    local className = LOCALIZED_CLASS_NAMES_MALE[player.class] or "Unkown Class"
    local raceName = ReGroup.RACE_NAMES[player.race] or "Unknown Race"

    button.name:SetText(player.name)
    button.name:SetTextColor(color.r, color.g, color.b, color.a)

    if player.race == "UNKNOWN" then
        button.info:SetText(className)
    else
        button.info:SetText(raceName .. " " .. className)
    end

    local checkButtonWidth = button:GetWidth() - button.checkButton:GetWidth() -- - 3 * button.tankIcon:GetWidth() - 8
    button.checkButton:SetChecked(player.checked and true or false)
    button.checkButton:SetHitRectInsets(0, -checkButtonWidth, 0, 0)
end

function ReGroup.ToggleImportListChecked(index)
    if index < 1 then return end
    local player = ReGroup.importList[index]
    if not player then return end
    player.checked = not player.checked
    ReGroup.ImportMembersFrame_Update()
end

function ReGroup.AddMembersFrame_OnButtonSelectAll()
    local allChecked = true
    for _, player in ipairs(ReGroup.importList) do
        if not player.checked then
            player.checked = true
            allChecked = false
        end
    end
    if allChecked then
        for _, player in ipairs(ReGroup.importList) do
            player.checked = false
        end
    end
    ReGroup.ImportMembersFrame_Update()
end

function ReGroup.AddMembersFrame_OnButtonImport()
    if not ReGroup.selectedGroup then return end
    for _, player in ipairs(ReGroup.importList) do
        if player.checked then
            ReGroup.AddMemberToSelectedGroup(player.name, player.class, player.race)
        end
    end
    ReGroup_ImportMembersFrame:Hide()
end