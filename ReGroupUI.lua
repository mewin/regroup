-------------------------------
--- ReGroup general UI code ---
-------------------------------

function ReGroup.SelectGroup(group)
    ReGroup.selectedGroup = group
    if group then
        UIDropDownMenu_SetText(ReGroupDropDownButton, group.name)

        for _, member in ipairs(group.members) do
            if not member.hasCharacterInfo then
                ReGroup.QueryMemberInfo(member)
            end
        end
    else
        UIDropDownMenu_SetText(ReGroupDropDownButton, "Select group ...")
    end

    ReGroup.ClearTable("selectedMembers")
    ReGroup.ClearTable("invitedMembers")
    ReGroup.ClearTable("importList")
    ReGroup.ClearTable("memberState")
    
    ReGroup.NotifyMembersChanged()
    ReGroup.UpdateFrame()
end

function ReGroup.RemoveSelectedMembers()
    if not ReGroup.selectedGroup then return end

    for i = #ReGroup.selectedGroup.members, 1, -1 do
        local member = ReGroup.selectedGroup.members[i]
        if ReGroup.selectedMembers[member.name] then
            table.remove(ReGroup.selectedGroup.members, i)
        end
    end
    table.wipe(ReGroup.selectedMembers)
    ReGroup.NotifyMembersChanged()
    ReGroup.UpdateFrame()
end

function ReGroup.InviteSelectedMembers(skipSizeCheck)
    if not ReGroup.selectedGroup then return end

    if not IsInRaid() and not skipSizeCheck then
        local numInvites = 0
        local inGroup = GetNumGroupMembers()
        for _, v in pairs(ReGroup.selectedMembers) do
            if v then numInvites = numInvites + 1 end
        end
        if inGroup + numInvites > 5 then
            StaticPopup_Show ("REGROUP_INVITE_CONVERT_TO_RAID")
            return
        end
    end

    local inviteTime = time()
    for _, member in ipairs(ReGroup.selectedGroup.members) do
        if ReGroup.selectedMembers[member.name] then
            if ReGroup.CanInvitePlayer(member) then
                ReGroup.SetMemberState(member.name, ReGroup.STATE_KEY_INVITE_STATE, INVITE_STATE_INVITED)
                ReGroup.SetMemberState(member.name, ReGroup.STATE_KEY_INVITE_TIME, inviteTime)
                
                if ReGroup.IsDebugName(member.name) then
                    print("Inviting debug char " .. member.name)
                else
                    C_PartyInfo.InviteUnit(member.name)
                end
            end
        end
    end
    ReGroup.NotifyMembersChanged()
    ReGroup.UpdateFrame()
end

function ReGroup.StartRename()
    if not ReGroup.selectedGroup then return end
    StaticPopup_Show ("REGROUP_RENAME_GROUP")
end

function ReGroup.StartRemove()
    if not ReGroup.selectedGroup then return end
    StaticPopup_Show ("REGROUP_REMOVE_GROUP")
end

function ReGroup.StartAddMember()
    if not ReGroup.selectedGroup then return end
    StaticPopup_Show ("REGROUP_ADD_MEMBER")
end

function ReGroup.StartRemoveMembers()
    if not ReGroup.selectedGroup then return end
    StaticPopup_Show ("REGROUP_REMOVE_MEMBERS")
end

function ReGroup.StartImportMembers()
    ReGroup_ImportMembersFrame:Show()
    ReGroup.ImportMembersFrame_SetFrom(ReGroup.IMPORT_FROM_FRIENDS)
end

function ReGroup.SelectAllMembers()
    if not ReGroup.selectedGroup then return end
    local allSelected = true
    for _, member in ipairs(ReGroup.selectedGroup.members) do
        if not ReGroup.selectedMembers[member.name] then
            allSelected = false
            break
        end
    end
    for _, member in ipairs(ReGroup.selectedGroup.members) do
        ReGroup.selectedMembers[member.name] = not allSelected
    end
    ReGroup.UpdateFrame()
end

function ReGroup.DropDownButton_OnLoad()
    info            = UIDropDownMenu_CreateInfo();

    local groups = ReGroup.GetGroups()
    for _, group in ipairs(groups) do
        info.text = group.name
        info.func = function()
            ReGroup.SelectGroup(group)
        end
        info.checked = group == ReGroup.selectedGroup
        UIDropDownMenu_AddButton(info)
    end
    info.text       = "New group ..."
    -- info.value      = "OptionVariable"
    info.func       = ReGroup.CreateGroup
    info.checked = false
    info.notCheckable = true
    
    UIDropDownMenu_AddButton(info)
end

function ReGroup.IsMemberFiltered(member)
    local searchText = ReGroup.TrimString(ReGroup_EditBoxSearch:GetText()):lower()

    if searchText ~= "" and not member.name:lower():find(searchText) then
        return true
    end

    return false
end

function ReGroup.NotifyMembersChanged()
    ReGroup.UpdateSortedMembers()
    ReGroup.UpdateMembersState()
end

function ReGroup.UpdateSortedMembers()
    table.wipe(ReGroup.sortedMembers)

    if not ReGroup.selectedGroup then return end
    for i, v in ipairs(ReGroup.selectedGroup.members) do
        if not ReGroup.IsMemberFiltered(v) then
            table.insert(ReGroup.sortedMembers, v)
        end
    end

    if ReGroupPersistent.sortBy ~= ReGroup.SORT_BY_ADDED then
        table.sort(ReGroup.sortedMembers, function(member0, member1)
            if ReGroupPersistent.sortBy == ReGroup.SORT_BY_ROLE and member0.role ~= member1.role then
                return ReGroup.ROLE_ORDER[member0.role] < ReGroup.ROLE_ORDER[member1.role]
            end
            return member0.name < member1.name
        end)
    end
end
