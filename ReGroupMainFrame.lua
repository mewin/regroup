--------------------------
--- ReGroup main frame ---
--------------------------

function ReGroup.Frame_OnSizeChanged()
    ReGroup.MembersScrollFrame_Update()

    HybridScrollFrame_CreateButtons(ReGroupMembersScrollFrame, "ReGroup_MemberListButtonTemplate")
    for _, button in ipairs(ReGroupMembersScrollFrame.buttons) do
        if not button.initDone then
            button.checkButton:RegisterForClicks("LeftButtonUp", "RightButtonUp");
            button.checkButton:SetScript("OnClick", function(self, btn)
                if btn == "LeftButton" then
                    ReGroup.ToggleMemberChecked(self:GetParent().index)
                elseif btn == "RightButton" then
                    ReGroup.UpdateMemberButton(self:GetParent()) -- CheckButton automatically changes, undo
                    ReGroup.ShowMemberContextMenu(self:GetParent().index)
                end
            end)
            button.initDone = true
        end
    end
end

function ReGroup.Frame_OnHide()
    ReGroup_ImportMembersFrame:Hide()
end

function ReGroup.Frame_OnEvent(self, event, ...)
    if event == "GROUP_ROSTER_UPDATE" then
        if ReGroup.convertToRaidRequested then
            ReGroup.TryConvertToRaid()
        end
        ReGroup.NotifyMembersChanged()
        ReGroup.UpdateFrame()
    elseif event == "CHAT_MSG_SYSTEM" then
        local text = ...
        local cannotInvitePlayer = text:match(ReGroup.CANNOT_INVITE_FORMAT)
        if cannotInvitePlayer then
            -- ReGroup.SetMemberState(cannotInvitePlayer, ReGroup.STATE_KEY_INGROUP, )
        end
    end
end

-----------------
-- member list --
-----------------

function ReGroup.MembersScrollFrame_Update()
    if not ReGroup_Frame:IsVisible() then return end

    local scrollFrame = ReGroupMembersScrollFrame
	local offset = HybridScrollFrame_GetOffset(scrollFrame)
	local buttons = scrollFrame.buttons
	local numButtons = #buttons
	local numMemberButtons = #ReGroup.sortedMembers

    local usedHeight = 0
    local height = 34
    local width = scrollFrame:GetWidth()

	for i = 1, numButtons do
		local button = buttons[i]
		local index = offset + i
		if ( index <= numMemberButtons ) then
			button.index = index
            ReGroup.UpdateMemberButton(button)

            button:SetHeight(height)
            button:SetWidth(width)
			usedHeight = usedHeight + height
			button:Show()
		else
			button.index = nil
			button:Hide()
		end
	end
	HybridScrollFrame_Update(scrollFrame, numMemberButtons * 34, usedHeight)
end

function ReGroup.ToggleMemberChecked(index)
    if index < 1 then return end
    if not ReGroup.selectedGroup then return end
    local member = ReGroup.sortedMembers[index]
    if not member then return end
    ReGroup.selectedMembers[member.name] = not ReGroup.selectedMembers[member.name]
    ReGroup.UpdateFrame()
end

function ReGroup.UpdateFrame()
    ReGroup.MembersScrollFrame_Update()

    local anyoneSelected = false
    for k, v in pairs(ReGroup.selectedMembers) do
        if v then
            anyoneSelected = true
            break
        end
    end

    if anyoneSelected then
        ReGroupButtonRemoveMembers:Enable()
        ReGroupButtonInviteMembers:Enable()
    else
        ReGroupButtonRemoveMembers:Disable()
        ReGroupButtonInviteMembers:Disable()
    end

    UIDropDownMenu_SetText(ReGroup_SortDropDown, ReGroup.SORT_BY_OPTIONS[ReGroupPersistent.sortBy].text)
end

-------------------------
-- member list entries --
-------------------------

function ReGroup.UpdateMembersState()
    if not ReGroup.selectedGroup then return end
    for _, member in ipairs(ReGroup.selectedGroup.members) do
        ReGroup.SetMemberState(member.name, ReGroup.STATE_KEY_INGROUP, ReGroup.IsInGroup(member.name)) -- or IsDebugName(member.name)
    end
end

function ReGroup.GetMemberState(name_, key, default)
    local name = ReGroup.NormalizeName(name_)
    local state = ReGroup.memberState[name]
    if not state or state[key] == nil then
        return default
    end
    return state[key]
end

function ReGroup.SetMemberState(name_, key, value)
    local name = ReGroup.NormalizeName(name_)
    if not ReGroup.memberState[name] then
        ReGroup.memberState[name] = {}
    end
    ReGroup.memberState[name][key] = value
end

local statusTable = {}

local function BuildStatus(member)
    local inviteState = ReGroup.GetMemberState(member.name, ReGroup.STATE_KEY_INVITE_STATE, ReGroup.INVITE_STATE_NONE)
    local inGroup = ReGroup.GetMemberState(member.name, ReGroup.STATE_KEY_INGROUP, false)
    table.wipe(statusTable)

    if inGroup then
        table.insert(statusTable, "In Group")
    elseif inviteState == ReGroup.INVITE_STATE_INVITED then
        local inviteTime = ReGroup.GetMemberState(member.name, ReGroup.STATE_KEY_INVITE_TIME, 0)
        table.insert(statusTable, date("Invited at %H:%M:%S", inviteTime))
    elseif inviteState == ReGroup.INVITE_STATE_FAILED then
        table.insert(statusTable, "Invite failed.")
    elseif inviteState == ReGroup.INVITE_STATE_DECLINED then
        table.insert(statusTable, "Invite declined.")
    end
end

local infoTable = {}

local function BuildInfo(member)
    local className = LOCALIZED_CLASS_NAMES_MALE[member.class] or "Unkown Class"
    local raceName = ReGroup.RACE_NAMES[member.race] or "Unknown Race"
    local isRaidAssistant = member[ReGroup.FLAG_RAID_ASSISTANT] or false
    local isMainTank = member[ReGroup.FLAG_RAID_MAIN_TANK] or false
    table.wipe(infoTable)

    table.insert(infoTable, raceName .. " " .. className)
    if isRaidAssistant then
        table.insert(infoTable, "Raid Assistant")
    end
    if isMainTank then
        table.insert(infoTable, "Main Tank")
    end
end

function ReGroup.UpdateMemberButton(button)
    if not ReGroup.selectedGroup then return end
    local index = button.index
    local member = ReGroup.sortedMembers[index]
    if not member then return end

    local color = RAID_CLASS_COLORS[member.class] or WHITE
    
    BuildStatus(member)
    BuildInfo(member)

    button.name:SetText(member.name)
    button.name:SetTextColor(color.r, color.g, color.b, color.a)

    button.status:SetText(table.concat(statusTable, ", "))
    button.info:SetText(table.concat(infoTable, ", "))

    local checkButtonWidth = button:GetWidth() - button.checkButton:GetWidth() - 3 * button.tankIcon:GetWidth() - 8
    button.checkButton:SetChecked(ReGroup.selectedMembers[member.name])
    button.checkButton:SetHitRectInsets(0, -checkButtonWidth, 0, 0)

    button.damagerIcon:SetAlpha(member.role == ReGroup.ROLE_DAMAGER and 1.0 or 0.5)
    button.healerIcon:SetAlpha(member.role == ReGroup.ROLE_HEALER and 1.0 or 0.5)
    button.tankIcon:SetAlpha(member.role == ReGroup.ROLE_TANK and 1.0 or 0.5)
end
function ReGroup.ShowMemberContextMenu(index)
    if index < 1 then return endurn end
    if not ReGroup.selectedGroup then return end
    local member = ReGroup.sortedMembers[index]
    if not member then return end

    -- TODO: dont reallocate menu
    local menu = {
        { text = "Select an Option", isTitle = true},
        { text = "Whisper", notCheckable = true, func = function() ReGroup.StartWhisper(member.name) end },
        { text = "Raid permissions", notCheckable = true, hasArrow = true,
            menuList = {
                {
                    text = "Assisant", checked = member[ReGroup.FLAG_RAID_ASSISTANT] or false,
                    func = function()
                        member[ReGroup.FLAG_RAID_ASSISTANT] = not member[ReGroup.FLAG_RAID_ASSISTANT]
                        ReGroup.ContextMenuFrame:Hide()
                        ReGroup.NotifyMembersChanged()
                        ReGroup.UpdateFrame()
                    end
                },
                {
                    text = "Main Tank", checked = member[ReGroup.FLAG_RAID_MAIN_TANK] or false,
                    func = function()
                        member[ReGroup.FLAG_RAID_MAIN_TANK] = not member[ReGroup.FLAG_RAID_MAIN_TANK]
                        ReGroup.ContextMenuFrame:Hide()
                        ReGroup.NotifyMembersChanged()
                        ReGroup.UpdateFrame()
                    end
                }
            } 
        }
    }
    
    -- TODO: cant this be reused?
    ReGroup.ContextMenuFrame = CreateFrame("Frame", "ReGroup_DropDownMenuFrame", UIParent, "UIDropDownMenuTemplate")
    EasyMenu(menu, ReGroup.ContextMenuFrame, "cursor", 0 , 0, "MENU");
end

function ReGroup.ToggleMemberRole(index, role)
    if index < 1 then return end
    if not ReGroup.selectedGroup then return end
    local member = ReGroup.sortedMembers[index]
    if not member then return end
    
    if role == member.role then
        member.role = "NONE"
    else
        member.role = role
    end
    ReGroup.NotifyMembersChanged()
    ReGroup.UpdateFrame()
end

----------------------------
-- group options dropdown --
----------------------------

function ReGroup.GroupOptionsDropDown_Initialize()
    
    local info = UIDropDownMenu_CreateInfo();
    info.notCheckable = true
    if not ReGroup.selectedGroup then
        info.disabled = true
    end

    info.text = "Rename"
    info.func = function()
        ReGroup.StartRename()
    end
    UIDropDownMenu_AddButton(info)

    info.text = "Remove"
    info.func = function()
        ReGroup.StartRemove()
    end
    UIDropDownMenu_AddButton(info)

    info.text = "Export ..."
    info.func = function()
        if not ReGroup.selectedGroup then return end

        ReGroup_SerializeFrame_Text.EditBox:SetText(ReGroup.SerializeGroup(ReGroup.selectedGroup))
        ReGroup_SerializeFrame:Show()
        ReGroup_ImportMembersFrame_ButtonImport:Disable()
    end
    UIDropDownMenu_AddButton(info)

    info.text = "Import ..."
    info.disabled = false
    info.func = function()
        ReGroup_SerializeFrame_Text.EditBox:SetText("")
        ReGroup_SerializeFrame:Show()
        ReGroup_ImportMembersFrame_ButtonImport:Enable()
    end
    UIDropDownMenu_AddButton(info)
end

ReGroupGroupOptionsMixin = {}

function ReGroupGroupOptionsMixin:OnLoad()
	SquareButton_SetIcon(self, "DOWN");
end

function ReGroupGroupOptionsMixin:OnMouseDown(button)
    UIMenuButtonStretchMixin.OnMouseDown(self, button)

    PlaySound(SOUNDKIT.IG_MAINMENU_OPTION_CHECKBOX_ON)
    
    ToggleDropDownMenu(nil, nil, self.DropDown, self, 0, 0)
end

-------------------------------------------
-- various actions on the selected group --
-------------------------------------------

function ReGroup.RenameSelectedGroup(name)
    if not ReGroup.IsGroupNameValid(name) then return end
    if not ReGroup.selectedGroup then return end

    ReGroup.RenameGroup(ReGroup.selectedGroup, name)
end

function ReGroup.RemoveSelectedGroup()
    if not ReGroup.selectedGroup then return end
    local groups = ReGroup.GetGroups()
    for k, v in ipairs(groups) do
        if v == ReGroup.selectedGroup then
            table.remove(groups, k)
            ReGroup.SelectFirstGroup()
            return
        end
    end
end

function ReGroup.AddMemberToSelectedGroup(name_, class, race)
    if not ReGroup.selectedGroup then return end
    ReGroup.AddMemberToGroup(ReGroup.selectedGroup, name_, class, race)
end

function ReGroup.FindMemberInSelectedGroup(name)
    if not ReGroup.selectedGroup then return nil end
    return ReGroup.FindMemberInGroup(ReGroup.selectedGroup, name)
end
