---------------------------------
--- ReGroup utility functions ---
---------------------------------

function ReGroup.IsDebugName(name)
    return name:sub(1, #ReGroup.DEBUG_PREFIX) == ReGroup.DEBUG_PREFIX
end

function ReGroup.RandomRace()
    return C_CreatureInfo.GetRaceInfo(math.random(37)).clientFileString
end

function ReGroup.RandomClass()
    return ReGroup.DEBUG_CLASS_NAMES[math.random(#ReGroup.DEBUG_CLASS_NAMES)]
end

function ReGroup.GenerateDebugMembers(group)
    for i = 1, 20 - #group.members do
        ReGroup.AddMemberToGroup(group, ReGroup.DEBUG_PREFIX .. "Character " .. i, ReGroup.RandomClass(), ReGroup.RandomRace())
    end
end

function ReGroup.StartWhisper(name)
    local eb = DEFAULT_CHAT_FRAME.editBox
    ChatEdit_ActivateChat(eb)
    eb:Insert("/whisper " .. name .. " ")
end

function ReGroup.TryConvertToRaid()
    if IsInGroup() and not IsInRaid() then
        C_PartyInfo.ConvertToRaid()
    end

    ReGroup.convertToRaidRequested = not IsInRaid()
end

function ReGroup.TrimString(str)
    -- from PiL2 20.4
    return (str:gsub("^%s*(.-)%s*$", "%1"))
end

function ReGroup.NormalizeName(name)
    if name:find("%-") then return name end
    return name .. "-" .. GetRealmName()
end

function ReGroup.IsInGroup(name)
    local charName = name:match("[^%-]*")
    if charName == "" then return false end
    return UnitInParty(charName) or UnitInRaid(charName)
end

function ReGroup.QueryMemberInfo(member)
    if UnitIsPlayer(member.name) then -- works for any player in the current group/raid
        local _, class = UnitClass(member.name)
        local _, race = UnitRace(member.name)
        member.class = class
        member.race = race
    else -- if C_FriendList.IsFriend(member.name) then
        if not member.class then member.class ="UNKNOWN" end
        if not member.race then member.race = "UNKNOWN" end
    end
end

function ReGroup.IsGroupNameValid(name)
    return ReGroup.TrimString(name) ~= ""
end

local lines = {}
function ReGroup.SerializeGroup(group)
    table.wipe(lines)

    table.insert(lines, "# This is your group in text form.")
    table.insert(lines, "# It can be stored and used to restore the group using the \"import\" option of ReGroup.")
    table.insert(lines, "")
    table.insert(lines, "[Group]")
    table.insert(lines, "Name=" .. group.name)

    for i, member in ipairs(group.members) do
        table.insert(lines, "")
        table.insert(lines, "[Member." .. i .. "]")
        table.insert(lines, "Name=" .. member.name)
        table.insert(lines, "Class=" .. member.class)
        table.insert(lines, "Race=" .. member.race)
        table.insert(lines, "Role=" .. member.role)
    end

    return table.concat(lines, "\n")
end

local group
local section = ""
local line = ""
local key = ""
local value = ""
local activeMember

function ReGroup.DeserializeGroup_ParseLine()
    if section == "Group" then
        if key == "Name" then
            group.name = value
        else
            print("Invalid key in Group section: " .. key)
        end
    elseif activeMember then
        if key == "Name" then
            activeMember.name = value
        elseif key == "Class" then
            activeMember.class = value
        elseif key == "Race" then
            activeMember.race = value
        elseif key == "Role" then
            activeMember.role = value
        else
            print("Invalid key in Member section: " .. key)
        end
    else
        print("Invalid line: " .. line)
    end
end

function ReGroup.DeserializeGroup(text)
    group = ReGroup.NewGroup()
    section = ""

    for line_ in string.gmatch(text .. "\n", "([^#\n]*)[^\n]-\n") do
        line = line_:match("^%s*(.-)%s*$") -- trim whitespace

        if line ~= "" then
            local v = line:match("^%[(.*)%]$")
            if v then
                section = v

                if section:sub(1, 6) == "Member" then
                    activeMember = ReGroup.AddMemberToGroup(group, "NoName-NoRealm")
                else
                    activeMember = nil
                end
            else
                key, value = line:match("^([^=]*)=(.*)$")
                if not key then
                    print("Invalid line: " .. line)
                else
                    ReGroup.DeserializeGroup_ParseLine()
                end
            end
        end
    end
    
    print("Successfully imported group.")
    ReGroup.SelectGroup(group)
end