
function ReGroup.SelectFirstGroup()
    local groups = ReGroup.GetGroups()
    if groups[1] then
        ReGroup.SelectGroup(groups[1])
    else
        ReGroup.SelectGroup(nil)
    end
end

--------------------
-- misc functions --
--------------------
function ReGroup.Init()
    C_GuildInfo.GuildRoster() -- might be required

    if not ReGroup.EventFrame then
        ReGroup.EventFrame = CreateFrame("Frame", nil, UIParent)
        ReGroup.EventFrame:SetFrameStrata("BACKGROUND")
        ReGroup.EventFrame:SetScript("OnEvent", ReGroup.Frame_OnEvent)
        ReGroup.EventFrame:SetWidth(1)
        ReGroup.EventFrame:SetHeight(1)
        ReGroup.EventFrame:SetPoint("TOPLEFT", 0, 0)
        ReGroup.EventFrame:Show()

        ReGroup.EventFrame:RegisterEvent("GROUP_ROSTER_UPDATE")
        ReGroup.EventFrame:RegisterEvent("CHAT_MSG_SYSTEM")
    end

    ReGroupMembersScrollFrame.update = ReGroup.MembersScrollFrame_Update
    HybridScrollFrame_CreateButtons(ReGroupMembersScrollFrame, "ReGroup_MemberListButtonTemplate")

    ReGroup_ImportMembersFrame_ScrollFrame.update = ReGroup.ImportMembersFrame_ScrollFrame_Update
    HybridScrollFrame_CreateButtons(ReGroup_ImportMembersFrame_ScrollFrame, "ReGroup_PlayerListButtonTemplate")
    for _, button in ipairs(ReGroup_ImportMembersFrame_ScrollFrame.buttons) do
        button.checkButton:SetScript("OnClick", function(self)
            ReGroup.ToggleImportListChecked(self:GetParent().index)
        end)
    end
end

function ReGroup.ClearTable(name)
    if not ReGroup[name] then
        ReGroup[name] = {}
    else
        table.wipe(ReGroup[name])
    end
end

-------------------
-- manage groups --
-------------------
function ReGroup.GetGroups()
    if not ReGroupPersistent.groups then
        ReGroupPersistent.groups = {}
    end
    return ReGroupPersistent.groups
end

function ReGroup.NewGroup()
    local groups = ReGroup.GetGroups()
    local newGroup = {}
    newGroup.name = "Unnamed group"
    newGroup.members = {}
    table.insert(groups, newGroup)
    return newGroup
end

function ReGroup.CreateGroup()
    local newGroup = ReGroup.NewGroup()
    ReGroup.SelectGroup(newGroup)
    ReGroup.StartRename()
end

function ReGroup.RenameGroup(group, name)
    if not ReGroup.IsGroupNameValid(name) then return end
    group.name = name
    if ReGroup.IsDebugName(name) then
        ReGroup.GenerateDebugMembers(group)
    end
    if group == ReGroup.selectedGroup then
        UIDropDownMenu_SetText(ReGroupDropDownButton, ReGroup.selectedGroup.name)
    end
end

-----------------------------
-- group member management --
-----------------------------
function ReGroup.AddMemberToGroup(group, name_, class, race)
    local name = ReGroup.NormalizeName(name_)
    if ReGroup.FindMemberInGroup(group, name) then return end
    local member = {
        name = name,
        hasCharacterInfo = false,
        class = class or "UNKNOWN",
        race = race or "UNKNOWN",
        role = "NONE"
    }
    table.insert(group.members, member)

    if group == ReGroup.selectedGroup then
        ReGroup.NotifyMembersChanged()
        ReGroup.UpdateFrame()
        ReGroup.QueryMemberInfo(member)
    end

    return member
end

function ReGroup.FindMemberInGroup(group, name)
    for _, member in ipairs(group.members) do
        if member.name == name then return member end
    end
    return nil
end

function ReGroup.CanInvitePlayer(member)
    local inGroupState = ReGroup.GetMemberState(member.name, ReGroup.STATE_KEY_INGROUP, false)
    -- local inviteState = ReGroup.GetMemberState(member.name, ReGroup.STATE_KEY_INVITE_STATE, ReGroup.INVITE_STATE_NONE)
    local inviteTime = ReGroup.GetMemberState(member.name, ReGroup.STATE_KEY_INVITE_TIME, 0.0)

    if not C_PartyInfo.CanInvite() then
        return false, "You cannot invite players to this party."
    elseif inGroupState then
        return false, "Player is already in this party."
    elseif inviteTime + ReGroup.INVITE_COOLDOWN > time() then
        return false, "Already invited this player."
    end
    return true
end
