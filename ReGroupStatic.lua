------------------------------------------
--- ReGroup static initialization code ---
------------------------------------------

if not ReGroupPersistent then
    ReGroupPersistent = {}
end

if not ReGroup then
    ReGroup = {}
end

ReGroup.DEBUG_PREFIX = "@DBG@"
ReGroup.DEBUG_CLASS_NAMES = {}

local WHITE = {r = 1.0, g = 1.0, b = 1.0, a = 1.0}
ReGroup.RACE_NAMES = {}
ReGroup.REVERSE_RACE_NAMES = {}
ReGroup.REVERSE_CLASS_NAMES = {}
ReGroup.IMPORT_FROM_GROUP = 1
ReGroup.IMPORT_FROM_FRIENDS = 2
ReGroup.IMPORT_FROM_GUILD = 3
ReGroup.IMPORT_FROM_CLUB = 100
ReGroup.IMPORT_FROM_LABELS = {
    [ReGroup.IMPORT_FROM_GROUP] = "From group ...",
    [ReGroup.IMPORT_FROM_FRIENDS] = "From friends ...",
    [ReGroup.IMPORT_FROM_GUILD] = "From guild ..."
}

ReGroup.SORT_BY_ADDED = 1
ReGroup.SORT_BY_CHARCTER_NAME = 2
ReGroup.SORT_BY_ROLE = 3
ReGroup.SORT_BY_OPTIONS = {
    [ReGroup.SORT_BY_ADDED]         = {text = "Added"},
    [ReGroup.SORT_BY_CHARCTER_NAME] = {text = "Character Name"},
    [ReGroup.SORT_BY_ROLE]          = {text = "Role (T>H>D)"}
}
ReGroup.ROLE_NONE = "NONE"
ReGroup.ROLE_TANK = "TANK"
ReGroup.ROLE_HEALER = "HEALER"
ReGroup.ROLE_DAMAGER = "DAMAGER"

ReGroup.ROLE_ORDER = {
    [ReGroup.ROLE_TANK] = 1,
    [ReGroup.ROLE_HEALER] = 2,
    [ReGroup.ROLE_DAMAGER] = 3,
    [ReGroup.ROLE_NONE] = 4
}

ReGroup.STATE_KEY_INGROUP = "inGroup"
ReGroup.STATE_KEY_INVITE_STATE = "inviteState"
ReGroup.STATE_KEY_INVITE_TIME = "inviteTime"
ReGroup.INVITE_STATE_NONE = "none"
ReGroup.INVITE_STATE_INVITED = "invited"
ReGroup.INVITE_STATE_FAILED = "failed"
ReGroup.INVITE_STATE_DECLINED = "declined"

ReGroup.INVITE_COOLDOWN = 5 -- seconds
ReGroup.CANNOT_INVITE_FORMAT = ERR_BAD_PLAYER_NAME_S:gsub("%%s", "(%%a+)")

ReGroup.FLAG_RAID_ASSISTANT = "flagRaidAssistant"
ReGroup.FLAG_RAID_MAIN_TANK = "flagRaidMainTank"

ReGroup.sortedMembers = {}

if not ReGroupPersistent.sortBy then
    ReGroupPersistent.sortBy = ReGroup.SORT_BY_ADDED
end

for i = 1, 100 do
    local raceInfo = C_CreatureInfo.GetRaceInfo(i)
    if raceInfo then
        if not ReGroup.RACE_NAMES[raceInfo.clientFileString] then -- there are multiple races with the same "clientFileNameString" (human/gilinean)
            ReGroup.RACE_NAMES[raceInfo.clientFileString] = raceInfo.raceName
        end
        if not ReGroup.REVERSE_RACE_NAMES[raceInfo.raceName] then
            ReGroup.REVERSE_RACE_NAMES[raceInfo.raceName] = raceInfo.clientFileString
        end
    end
end

for k, v in pairs(LOCALIZED_CLASS_NAMES_MALE) do
    ReGroup.REVERSE_CLASS_NAMES[v] = k
    table.insert(ReGroup.DEBUG_CLASS_NAMES, k)
end

for k, v in pairs(LOCALIZED_CLASS_NAMES_FEMALE) do
    ReGroup.REVERSE_CLASS_NAMES[v] = k
end

SLASH_REGROUP1 = "/regroup"
SLASH_REGROUP2 = "/rgrp"

SlashCmdList["REGROUP"] = function(msg, editBox)
    ReGroup_Frame:Show()
    if not ReGroup.selectedGroup then
        ReGroup.SelectFirstGroup()
    end
    ReGroup.UpdateFrame()
end

StaticPopupDialogs["REGROUP_RENAME_GROUP"] = {
    text = "Renaming group",
    button1 = "Okay",
    button2 = "Cancel",
    OnShow = function (self, data)
        if not ReGroup.selectedGroup then return end
        UIDropDownMenu_DisableDropDown(ReGroupDropDownButton)
        self.editBox:SetText(ReGroup.selectedGroup.name)
    end,
    OnAccept = function(self, data, data2)
        local text = self.editBox:GetText()
        ReGroup.RenameSelectedGroup(text)
    end,
    OnHide = function()
        UIDropDownMenu_EnableDropDown(ReGroupDropDownButton)
    end,
    EditBoxOnTextChanged = function (self, data)   -- careful! 'self' here points to the editbox, not the dialog
        if ReGroup.IsGroupNameValid(self:GetText()) then
            self:GetParent().button1:Enable()
        else
            self:GetParent().button1:Disable()
        end
    end,
    enterClicksFirstButton = true,
    hasEditBox = true,
    timeout = 0,
    whileDead = true,
    hideOnEscape = true,
    preferredIndex = 3,  -- avoid some UI taint, see http://www.wowace.com/announcements/how-to-avoid-some-ui-taint/
}

StaticPopupDialogs["REGROUP_REMOVE_GROUP"] = {
    text = "Remove group?",
    button1 = "Okay",
    button2 = "Cancel",
    OnShow = function ()
        UIDropDownMenu_DisableDropDown(ReGroupDropDownButton)
    end,
    OnAccept = function()
        ReGroup.RemoveSelectedGroup()
    end,
    OnHide = function()
        UIDropDownMenu_EnableDropDown(ReGroupDropDownButton)
    end,
    timeout = 0,
    whileDead = true,
    hideOnEscape = true,
    preferredIndex = 3,  -- avoid some UI taint, see http://www.wowace.com/announcements/how-to-avoid-some-ui-taint/
}

StaticPopupDialogs["REGROUP_ADD_MEMBER"] = {
    text = "Add member...",
    button1 = "Okay",
    button2 = "Cancel",
    OnShow = function ()
        UIDropDownMenu_DisableDropDown(ReGroupDropDownButton)
    end,
    OnAccept = function(self, data, data2)
        local text = self.editBox:GetText()
        ReGroup.AddMemberToSelectedGroup(text)
    end,
    OnHide = function()
        UIDropDownMenu_EnableDropDown(ReGroupDropDownButton)
    end,
    EditBoxOnTextChanged = function (self, data)   -- careful! 'self' here points to the editbox, not the dialog
        if self:GetText() ~= "" then
            self:GetParent().button1:Enable()
        else
            self:GetParent().button1:Disable()
        end
    end,
    enterClicksFirstButton = true,
    hasEditBox = true,
    timeout = 0,
    whileDead = true,
    hideOnEscape = true,
    preferredIndex = 3,  -- avoid some UI taint, see http://www.wowace.com/announcements/how-to-avoid-some-ui-taint/
}

StaticPopupDialogs["REGROUP_REMOVE_MEMBERS"] = {
    text = "Remove selected members from group?",
    button1 = "Okay",
    button2 = "Cancel",
    OnShow = function ()
        UIDropDownMenu_DisableDropDown(ReGroupDropDownButton)
    end,
    OnAccept = function()
        ReGroup.RemoveSelectedMembers()
    end,
    OnHide = function()
        UIDropDownMenu_EnableDropDown(ReGroupDropDownButton)
    end,
    timeout = 0,
    whileDead = true,
    hideOnEscape = true,
    preferredIndex = 3,  -- avoid some UI taint, see http://www.wowace.com/announcements/how-to-avoid-some-ui-taint/
}

StaticPopupDialogs["REGROUP_INVITE_CONVERT_TO_RAID"] = {
    text = "Convert group to raid?",
    button1 = "Yes",
    button2 = "No",
    OnShow = function ()
        UIDropDownMenu_DisableDropDown(ReGroupDropDownButton)
    end,
    OnAccept = function()
        ReGroup.TryConvertToRaid()
        ReGroup.InviteSelectedMembers(true)
    end,
    OnCancel = function()
        ReGroup.InviteSelectedMembers(true)
    end,
    OnHide = function()
        UIDropDownMenu_EnableDropDown(ReGroupDropDownButton)
    end,
    timeout = 0,
    whileDead = true,
    hideOnEscape = true,
    preferredIndex = 3,  -- avoid some UI taint, see https://web.archive.org/web/20111203140234/https://www.tukui.org/forums/topic.php?id=15823
}
